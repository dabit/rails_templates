gem 'after_party', '~> 1.11'
gem 'bullet', '~> 7.0'
gem 'devise', '~> 4.8.1'
gem 'devise-async', '~> 1.0'
gem 'devise-i18n', '~> 1.10'
gem 'devise_invitable', '~> 2.0'
gem 'kaminari', '~> 1.2'
gem 'money-rails', '~> 1.15'
gem 'noticed', '~> 1.5'
gem 'pundit', '~> 2.1'
gem 'responders', github: 'heartcombo/responders'
gem 'sidekiq', '~> 6.3'

gem_group :development, :test do
  gem 'factory_bot_rails', '~> 6.2'
  gem 'ffaker', '~> 2.21'
  gem 'i18n-debug', '~> 1.2'
  gem 'letter_opener', '~> 1.7'
  gem 'minitest-spec-rails', '~> 6.2.0'
  gem 'pry-rails', '~> 0.3.9'
  gem 'rubocop-rails', '~> 2.17'
  gem 'ruby-lsp', '~> 0.3.0'
  gem 'simplecov', '~> 0.21.2', require: false
  gem 'simplecov-lcov', '~> 0.8.0', require: false
end

gem_group :development do
  gem 'annotate'
  gem 'brakeman', '~> 5.3'
  gem 'i18n_yaml_sorter'
  gem 'solargraph'
end

run_bundle


initializer 'generators.rb', <<-'CODE'
Rails.application.config.generators do |g|
  g.orm :active_record, primary_key_type: :uuid
  g.system_tests = nil
  g.stylesheets false
  g.helper false
  g.jbuilder false
end
CODE

generate('devise:install')
generate('after_party:install')
generate('bullet:install')
generate('devise:i18n:locale', 'es')
generate('devise:i18n:locale', 'en')
generate('devise:i18n:views')
generate('kaminari:config')
generate('pundit:install')
generate('responders:install')

environment "config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }", env: 'development'
environment 'config.action_mailer.delivery_method = :letter_opener', env: 'development'
environment "config.action_controller.asset_host = 'http://localhost:3000'", env: 'development'
environment "config.action_mailer.asset_host = 'http://localhost:3000'", env: 'development'
environment 'config.action_mailer.default_url_options = {host: "http://test.com"}', env: 'test'

file 'db/20230316225948_enable_extension_for_uuid.rb', <<-CODE
class EnableExtensionForUuid < ActiveRecord::Migration[7.0]
  def change
    enable_extension 'pgcrypto'
  end
end
CODE

file '.rubocop.yml', <<-CODE
require:
  - rubocop-rails

AllCops:
  NewCops: enable
  Include:
    - 'Gemfile'
    - 'app/controllers/**/*.rb'
    - 'app/models/**/*.rb'
    - 'app/services/**/*.rb'
    - 'app/jobs/**/*.rb'
    - 'app/mailers/**/*.rb'
    - 'app/notifications/**/*.rb'
    - 'test/**/*.rb'
    - 'config/initializers/**/*.rb'
    - 'config/routes.rb'

Style/Documentation:
  Enabled: false

Layout/LineLength:
  Exclude:
    - 'config/routes.rb'
  Max: 130

Naming/MethodParameterName:
  AllowedNames:
    - f
    - id

Rails/SkipsModelValidations:
  Enabled: false

Metrics/BlockLength:
  Exclude:
    - 'config/routes.rb'
  AllowedMethods:
    - draw
    - use_cassette
    - task
    - test
    - configure
    - describe
    - it
    - context
    - namespace
    - factory
    - define
    - let

Metrics/ClassLength:
  Max: 300

Metrics/MethodLength:
  Max: 15
CODE

rakefile 'annotate_models.rake' do
  <<-CODE
if Rails.env.development?
  require 'annotate'

  task routes: :environment do
    require 'rails/commands/routes/routes_command'
    Rails.application.require_environment!
    cmd = Rails::Command::RoutesCommand.new
    cmd.perform
  end

  task set_annotation_options: :environment do
    # You can override any of these by setting an environment variable of the
    # same name.
    Annotate.set_defaults(
      'active_admin' => 'false',
      'additional_file_patterns' => [],
      'routes' => 'true',
      'models' => 'true',
      'position_in_routes' => 'before',
      'position_in_class' => 'before',
      'position_in_test' => 'before',
      'position_in_fixture' => 'before',
      'position_in_factory' => 'before',
      'position_in_serializer' => 'before',
      'show_foreign_keys' => 'true',
      'show_complete_foreign_keys' => 'false',
      'show_indexes' => 'true',
      'simple_indexes' => 'false',
      'model_dir' => 'app/models',
      'root_dir' => '',
      'include_version' => 'false',
      'require' => '',
      'exclude_tests' => 'false',
      'exclude_fixtures' => 'false',
      'exclude_factories' => 'false',
      'exclude_serializers' => 'false',
      'exclude_scaffolds' => 'true',
      'exclude_controllers' => 'true',
      'exclude_helpers' => 'true',
      'exclude_sti_subclasses' => 'false',
      'ignore_model_sub_dir' => 'false',
      'ignore_columns' => nil,
      'ignore_routes' => nil,
      'ignore_unknown_models' => 'false',
      'hide_limit_column_types' => 'integer,bigint,boolean',
      'hide_default_column_types' => 'json,jsonb,hstore',
      'skip_on_db_migrate' => 'false',
      'format_bare' => 'true',
      'format_rdoc' => 'false',
      'format_yard' => 'false',
      'format_markdown' => 'false',
      'sort' => 'false',
      'force' => 'false',
      'frozen' => 'false',
      'classified_sort' => 'true',
      'trace' => 'false',
      'wrapper_open' => nil,
      'wrapper_close' => nil,
      'with_comment' => 'true'
    )
  end

  Annotate.load_tasks
end
  CODE
end

file 'app/models/application_record.rb', <<-'CODE'
class ApplicationRecord < ActiveRecord::Base
  primary_abstract_class

  def self.inherited(subclass)
    super
    return unless subclass.has_attribute?(:deleted_at)

    setup_for_soft_delete(subclass)
  rescue ActiveRecord::NoDatabaseError, ActiveRecord::StatementInvalid
    nil
  end

  def self.setup_for_soft_delete(subclass)
    subclass.send(:default_scope, -> { where(deleted_at: nil) })

    class << subclass
      def archived
        where.not(deleted_at: nil)
      end
    end

    subclass.define_method(:destroy) do
      touch(:deleted_at)
    end
  end

  def self.human_enum_name(enum_name, enum_value)
    I18n.t("activerecord.attributes.#{model_name.i18n_key}.#{enum_name.to_s.pluralize}.#{enum_value}")
  end

  def self.plural_name
    model_name.human(count: 2)
  end

  def self.enum_collection_for_select(enum_plural_name)
    send(enum_plural_name).collect { |k, _v| [k, human_enum_name(enum_plural_name, k)] }
  end
end
CODE

initializer 'bullet.rb', <<-'CODE'
if Rails.env.development?
  Bullet.enable = true
  Bullet.bullet_logger = true
  Bullet.console = true
  Bullet.rails_logger = true
  Bullet.add_footer = true
  Bullet.skip_html_injection = false
end
CODE

file 'lib/services/services/application_service.rb', <<-'CODE'
module Services
  class ApplicationService
    def self.call(**args)
      new.call(**args)
    end
  end
end
CODE

file 'lib/generators/service/service_generator.rb', <<-'CODE'
class ServiceGenerator < Rails::Generators::NamedBase
  source_root File.expand_path("templates", __dir__)

  def create_service_file
    template 'service.rb.erb', "app/services/services/#{file_path}.rb"
  end

  def create_test_file
    template 'service_test.rb.erb', "test/services/#{file_path}_test.rb"
  end
end
CODE

file 'lib/generators/service/templates/service.rb.erb', <<-'CODE'
class Services::<%=class_name %> < ApplicationService
  def call(params:)

  end
end
CODE

file 'lib/generators/service/templates/service_test.rb.erb', <<-'CODE'
require 'test_helper'

class Services::<%= class_name %>Test < ActiveSupport::TestCase
  describe 'call' do

  end
end
CODE

file 'test/lib/generators/service_generator_test.rb', <<-'CODE'
# frozen_string_literal: true

require 'test_helper'
require 'generators/service/service_generator'

class ServiceGeneratorTest < Rails::Generators::TestCase
  tests ServiceGenerator
  destination Rails.root.join('tmp/generators')
  setup :prepare_destination

  test 'generator runs without errors' do
    assert_nothing_raised do
      run_generator ['projects/create']
    end
  end

  test 'should add the service file to app/services/services' do
    run_generator ['projects/create']

    assert_file 'app/services/services/projects/create.rb'
  end

  test 'should add the service test file to test/services' do
    run_generator ['projects/create']

    assert_file 'test/services/projects/create_test.rb'
  end
end
CODE

run 'bundle exec rubocop -a'
